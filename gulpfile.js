var gulp = require('gulp');
var gutil = require('gulp-util');
var sass = require('gulp-sass');
var uglify = require('gulp-uglify');
var plumber = require ('gulp-plumber');
var rename = require('gulp-rename');
var sourcemaps = require('gulp-sourcemaps');

var jsSources = ['js/**/*.js'];
var sassSources = ['sass/**/*.scss'];
var outputDir = 'dist/';

// report errors but continue gulpin'
var onError = function (err) {
  gutil.beep();
  console.log(err.messageFormatted);
  this.emit('end'); // super crit
};

// Compile scss into css files to style site output
var sassBuild = function() {
  return gulp.src(sassSources)
    .pipe(plumber({
      errorHandler: onError
    }))
    .pipe(sourcemaps.init())
    .pipe(sass())
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(outputDir + 'css'));
};

// Rename, minify, and move Javascript files
var jsBuild = function() {
  return gulp.src(jsSources)
    .pipe(plumber({
      errorHandler: onError
    }))
    .pipe(rename(function (path) {
      path.basename += ".min";
      path.extname = ".js";
    }))
    .pipe(uglify())
    .pipe(gulp.dest(outputDir + 'js'));
};

// create tasks from our functions saved into variables
gulp.task('sass',sassBuild);
gulp.task('js',jsBuild);

// watch for js and style changes
gulp.task('watch', function() {
  gulp.watch(jsSources, ['js']);
  gulp.watch(sassSources, ['sass']);
});

// Server Tasks
// run sass (compile scss into css) and js (minify js files) right away, and start watching for new changes
gulp.task('default', ['sass','js','watch']);

