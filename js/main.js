window.addEventListener("load", function() {
  var title = $('h1.title');

  $('div.home').removeClass('hidden');
  $('#resultDetails').addClass('hidden');

  var searchResults = $("#searchResults");

  var searchRequest;
  var searchField = $("#searchbox");

  var searchIcon = $(".search-icon .icon");
  searchIcon.click(function() {
    searchField.toggleClass('hide');
  });

}, false);

function miscFunctions() {
  $('#searchResults li').each(function() {
    var heights = [$(this).find('.front').height(),$(this).find('.back').height()];
    $(this).find('.item').height(Math.max.apply( Math, heights ));
  });

  $('#searchResults li p.flip').click(function() {
    item_id = $(this).parent().parent().attr('id');

    $('#' + item_id + ' .item').addClass('flipped');
    $('#' + item_id).addClass('open');

    $('#' + item_id + ' .reverse-flip').click(function() {
      $(this).parent().parent().removeClass('flipped');
      $(this).parent().parent().parent().removeClass('open');
    });
  });
}
