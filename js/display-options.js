window.addEventListener("load", function() {
  var searchResults = $("#searchResults");

	var list_view = $(".display-options .list-style--options .form-item .option.list");
  var grid_view = $(".display-options .list-style--options .form-item .option.grid");
  list_view.click(function() {
    if (searchResults.hasClass('grid')) {
      searchResults.toggleClass('grid');
      searchResults.toggleClass('list');
    }
    $('#searchResults li').matchHeight();
  });
  grid_view.click(function() {
    if (searchResults.hasClass('list')) {
      searchResults.toggleClass('list');
      searchResults.toggleClass('grid');
    }
    $('#searchResults li').matchHeight();
  });
});