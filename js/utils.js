// JavaScript Document

//utility function to create AJAX request/XHR object

function createRequest() {
	
	try {
		request = new XMLHttpRequest();
	} catch(e) {
		try {
			request = new ActiveXObject("Msxml2.XMLHTTP");
		} catch(e) {
			try {
		 		request = new ActiveXObject("Microsoft.XMLHTTP");
			} catch(e) {
				request = null;
			}
		}
	}
	
	return request;
}

function getTransformValues(q) {
	var el = document.querySelector(q);
	var st = window.getComputedStyle(el, null);
	var tr = st.getPropertyValue("-webkit-transform") ||
	         st.getPropertyValue("-moz-transform") ||
	         st.getPropertyValue("-ms-transform") ||
	         st.getPropertyValue("-o-transform") ||
	         st.getPropertyValue("transform") ||
	         "FAIL";

	// rotation matrix - http://en.wikipedia.org/wiki/Rotation_matrix
	if (tr !== 'none') {
		var values = tr.split('(')[1].split(')')[0].split(',');
		var a = values[0];
		var b = values[1];
		var c = values[2];
		var d = values[3];

		var scale = Math.sqrt(a*a + b*b);

		// console.log('Scale: ' + scale);

		// arc sin, convert from radians to degrees, round
		var sin = b/scale;
		// next line works for 30deg but not 130deg (returns 50);
		// var angle = Math.round(Math.asin(sin) * (180/Math.PI));
		var angle = Math.round(Math.atan2(b, a) * (180/Math.PI));

		// console.log('Rotate: ' + angle + 'deg');

		var transform = {};
		transform['rotate'] = angle;
		transform['scale'] = scale;
		transform['matrix'] = tr;
		return transform;
	}

}
