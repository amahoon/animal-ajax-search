window.addEventListener("load", function() {
  $("form#filters input").each(function() {
    $(this).change(function() {
      var inputs = $("[name='" + $(this).attr('name') + "']").not(this);
      if ($(this).is(':checked')) {
        inputs.each(function() {
          $(this).attr('disabled','disabled');
          $(this).parent().toggleClass('disabled');
        });
      } else {
        inputs.each(function() {
          $(this).removeAttr('disabled');
          $(this).parent().toggleClass('disabled');
        });
      }

      showFilteredResults($('form#filters').serialize());
    });
  });

  $("#searchbox").keyup(function() {
    showFilteredResults($('form#filters').serialize());
  });

  $('#searchResults li').matchHeight();

  miscFunctions();
});

function showFilteredResults(str) {
  var url = "includes/views/listAnimals.php";
  search = $('#searchbox').val();
  url += (search.length > 0) ? '?search=' + search : '';
  if (str.length > 0) {
    if (url.indexOf('?') > -1) {
      url += '&' + str;
    } else {
      url += '?' + str;
    }
  };

  searchRequest = createRequest(); // function in the utils.js file
  if (searchRequest == null) {
    alert("Browser does not support HTTP Request");
    return;
  }

  searchRequest.onreadystatechange = filterStatus; // filterStatus is a function
  searchRequest.open("GET", url, true);
  searchRequest.send(null);
}

function filterStatus() {
  if (searchRequest.readyState == 4 || searchRequest.readyState == "complete") {
    searchResults.innerHTML = searchRequest.responseText;
    $('#searchResults li').matchHeight();
    $('div.home').removeClass('hidden');
    $('#resultDetails').addClass('hidden');

    miscFunctions();
  }
}

function displayInfo(id) {
  $('div.home').addClass('hidden');
  $('#resultDetails').removeClass('hidden');

  $('#searchResults li p.flip').each(function() {
    item_id = $(this).parent().parent().attr('id');
    
    $('#' + item_id + ' .reverse-flip').parent().parent().removeClass('flipped');
    $('#' + item_id + ' .reverse-flip').parent().parent().parent().removeClass('open');
  });

  var displayRequest;

  function displayAnimal(id) {
    displayRequest = createRequest();

    if (displayRequest == null) {
      alert("Browser not supported, please upgrade");
      return;
    }

    var url = "includes/views/animal.php?id="+id;
    displayRequest.onreadystatechange = displayStatus;
    displayRequest.open("GET", url, true);
    displayRequest.send(null);
  }

  function displayStatus() {
    if (displayRequest.readyState == 4 || displayRequest.readyState == "complete") {
      $('#detailsLoc').html(displayRequest.responseText);
    }
  }

  displayAnimal(id);
}