<?php
  $base_url = $_SERVER['DOCUMENT_ROOT'];
  include $base_url . '/includes/config.php';

  error_reporting( E_ALL );
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Live Search | Animals</title>
<link href="dist/css/styles.css" rel="stylesheet" type="text/css" media="screen">
</head>
<body>
  <!-- <div class="loading">
    <div id="loader"><div id="inner"></div></div>
  </div> -->
  <div class="home">
    <header>
      <div class="search">
        <div class="search-icon"><p class="icon"></p></div>
        <h1 class="title">Take a Walk on the Wild Side</h1>
        <form>
          <input type="search" name="searchbox" id="searchbox" class="hide" placeholder="enter search keyword...">
        </form>
      </div>
      <div class="date">
        <p><span class="day"><?=$day?></span><span class="suffix"><?=$suffix?></span><br><span class="month"><?=$month?></span></p>
        <img class="season-icon season--<?=$season?>" src="images/icons/<?=$season?>-icon.png">
      </div>
    </header>

    <section class="all-animals">
      <h2 class="hidden">Animals</h2>
      <?php // include $base_url . '/includes/views/displayOptions.php'; ?>
      <?php include $base_url . '/includes/views/searchFormFilters.php'; ?>
      <div id="searchResults" class="grid">
        <?php include $base_url . '/includes/views/listAnimals.php'; ?>
      </div>
      <div class="background">
        <img src="images/backgrounds/seasons/<?=$season?>-bkgd.jpg" alt="background">
      </div>
    </section>
  </div>

  <section id="resultDetails" class="hidden">
    <h3 class="hidden">Animal Details</h2>
    <div id="detailsLoc"></div>
  </section>

  <?php include $base_url . '/includes/scripts.php'; ?>
</body>
</html>