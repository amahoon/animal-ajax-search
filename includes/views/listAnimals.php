<?php
  $base_url = $_SERVER['DOCUMENT_ROOT'];
  include $base_url . '/includes/config.php';

  $filters = $_GET;
  $animals = new Animals();

  $results = $animals->filter($filters);
?>

<?php if (!empty($results)) { ?>
  <ul>
  <?php foreach ($results as $row) { ?>
    <li id="item--<?=$row['id']?>">
      <div class="flip item">
        <div class="front">
          <img class="profile-image" src="images/thumbnails/<?=$row['thumb']?>">
          <div class="info-container">
            <p class="name"><?=$row['animal']?></p>
            <div class="information">
              <?php if ($row['status']) { ?>
                <p class="conservation-status"><span class="label">Status: </span><?=$row['status']?></p>
              <?php } ?>
              <?php if ($row['diet']) { ?>
                <p class="diet"><span class="label">Diet: </span><?=$row['diet']?></p>
              <?php } ?>
              <?php if ($row['type']) { ?>
                <p class="type"><span class="label">Type: </span><?=$row['type']?></p>
              <?php } ?>
              <?php if ($row['lifestyle']) { ?>
                <p class="lifestyle"><span class="label">Lifestyle: </span><?=implode(', ',$row['lifestyle'])?></p>
              <?php } ?>
            </div>
          </div>
        </div>
        <div class="back">
          <p class="reverse-flip">close</p>
          <div class="information">
            <?php if ($row['description']) { ?>
              <?php $str = strip_tags(str_replace('  ',' ',str_replace('<br>',' ',$row['description']))); ?>
              <p class="desc short"><?=trim(substr($str,0,350))?><?=(strlen($str) > 350) ? '...' : ''?></p>
              <p class="desc long"><?=trim(substr($str,0,800))?><?=(strlen($str) > 800) ? '...' : ''?></p>
            <?php } ?>
          </div>
          <a href="#" onclick="displayInfo(<?=$row['id']?>)" class="continue-reading">continue reading</a>
        </div>
      </div>
      <div class="page-curl">
        <div class="curl"></div>
        <p class="flip"></a>
      </div>
    </li>
  <?php } ?>
  </ul>
<?php } else { ?>
  <div class="no-results"><p>There are currently no animals available. Please try again later.</p></div>
<?php } ?>
