<?php

  $base_url = $_SERVER['DOCUMENT_ROOT'];
  include $base_url . '/includes/config.php';
  
  $filters = new Filters();
  $values = $filters->getFilerValues();

?>

<div class="filter-options">
	<div class="container">
	  <form method="post" accept-charset="utf-8" id="filters">
      <?php foreach ($values as $name => $field) { ?>
  	    <div class="form-item <?=$name?>">
          <label class="item-label <?=$name?>"><?=ucfirst($name)?></label>
          <?php foreach ($field as $id => $value) { ?>
            <div class="option">
              <label><input type="checkbox" name="<?=$name?>" value="<?=$id?>"><?=$value?></label>
            </div>
          <?php } ?>
  	    </div>
      <?php } ?>
	  </form>
	</div>
</div>