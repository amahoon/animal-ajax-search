<?php
  $base_url = $_SERVER['DOCUMENT_ROOT'];
  include $base_url . '/includes/config.php';

  $id = $_GET['id'];
  $animals = new Animals();
  $results = $animals->get($id);
?>

<?php if (!empty($results)) { ?>
  <?php foreach ($results as $row) { ?>
    <div id="details">
    	<div class="header">
    	  <img class="profImg" src="images/thumbnails/<?=$row['thumb']?>" alt="<?=$row['animal']?> Profile Image">
        <div class="specs">
          <h3 class="profName"><?=$row['animal']?></h3>
          <ul>
            <li>
    		      <p class="conservation-status"><span class="label">Status: </span><?=$row['status']?></p>
            </li>
            <?php if ($row['scientific_name']) { ?>
              <li>
      		      <p class="scientific-name"><span class="label">Scientific Name: </span><?=$row['scientific_name']?></p>
              </li>
            <?php } ?>
            <?php if ($row['type']) { ?>
              <li>
                <p class="type"><span class="label">Type: </span><?=$row['type']?></p>
              </li>
            <?php } ?>
            <?php if ($row['diet']) { ?>
              <li>
                <p class="diet"><span class="label">Diet: </span><?=$row['diet']?></p>
              </li>
            <?php } ?>
            <?php if ($row['size__length']) { ?>
              <li>
                <p class="size--length"><span class="label">Size (length): </span><br><?=$row['size__length']?></p>
              </li>
            <?php } ?>
            <?php if ($row['size__height']) { ?>
              <li>
                <p class="size--height"><span class="label">Size (height): </span><br><?=$row['size__height']?></p>
              </li>
            <?php } ?>
            <?php if ($row['weight']) { ?>
              <li>
                <p class="weight"><span class="label">Weight: </span><br><?=$row['weight']?></p>
              </li>
            <?php } ?>
            <?php if ($row['top_speed']) { ?>
              <li>
                <p class="top-speed"><span class="label">Top Speed: </span><?=$row['top_speed']?></p>
              </li>
            <?php } ?>
            <?php if ($row['life_span']) { ?>
              <li>
                <p class="life-span"><span class="label">Life Span: </span><?=$row['life_span']?></p>
              </li>
            <?php } ?>
            <?php if ($row['lifestyle']) { ?>
              <li>
        		    <p class="lifestyle"><span class="label">Lifestyle: </span><?=implode(', ',$row['lifestyle'])?></p>
              </li>
            <?php } ?>
          </ul>
    		</div>
    	</div>
    	<div id="descHolder">
    	  <p id="animalDesc"><?=$row['description']?></p>
    	</div>
    </div>

    <div id="goBack">
    	<a href=""><img src="images/goBack.png" alt="Go Back"></a>
    </div>

    <div class="background">
        <img src="images/backgrounds/<?=$row['bkgd']?>" alt="background">
    </div>
  <?php } ?>
<?php } ?>