<?php

/**
 * @file
 * Creates an object with method calls related to animals.
 */

class Animals {

  public function connect() {
    return new Connection;
  }

	public function search($str = '') {
    $connection = $this->connect();

    $connection->select('animals','an');
    $connection->leftJoin('field_image__thumb','thumbs','thumbs.animal_id = an.id');
    $connection->leftJoin('field_image__large','bkgd','bkgd.animal_id = an.id');
    $connection->leftJoin('field_type','ty','ty.animal_id = an.id');
    $connection->leftJoin('field_status','s','s.animal_id = an.id');
    $connection->leftJoin('field_diet','d','d.animal_id = an.id');
    $connection->leftJoin('terms','t_ty','t_ty.id = ty.term_id');
    $connection->leftJoin('terms','t_st','t_st.id = s.term_id');
    $connection->leftJoin('terms','t_diet','t_diet.id = d.term_id');
    $connection->fields('an',array('id'));
    $connection->addField('an','name','animal');
    $connection->addField('t_ty','name','type');
    $connection->addField('t_st','name','status');
    $connection->addField('t_diet','name','diet');
    $connection->fields('an',array('description'));
    $connection->addField('thumbs','filename','thumb');
    $connection->addField('bkgd','filename','bkgd');
    if (strlen($str) > 0) {
      $connection->condition('an.name','%' . $str . '%','LIKE');
    }
    $connection->orderBy('animal');
    $connection->groupBy('an.id');

    $res = $connection->execute();
    $results = $connection->arrayResults($res);

    if (!empty($results)) {
      foreach ($results as $key => $row) {
        $lifestyle = $this->getLifestyle($row['id']);
        if ($lifestyle) {
          $results[$key]['lifestyle'] = array();
          foreach ($lifestyle as $ls) {
            $results[$key]['lifestyle'][] = $ls['name'];
          }
        }
      }
    }

    return $results;
	}

  public function filter($filters) {
    // echo '<div style="background-color: white;"><pre>' . var_export($filters,TRUE) . '</pre></div>';
    $filter = new Filters;
    $connection = $this->connect();

    $connection->select('animals','an');
    $connection->leftJoin('field_image__thumb','thumbs','thumbs.animal_id = an.id');
    $connection->leftJoin('field_image__large','bkgd','bkgd.animal_id = an.id');
    $connection->fields('an',array('id'));
    $connection->addField('an','name','animal');
    $connection->fields('an',array('description'));
    $connection->addField('thumbs','filename','thumb');
    $connection->addField('bkgd','filename','bkgd');
    foreach ($filter->getVocabs() as $filter) {
      foreach ($filter as $value) {
        $connection->leftJoin('field_' . $value,'field_' . $value,'field_' . $value . '.animal_id = an.id');
        $connection->leftJoin('terms','t_' . $value,'t_' . $value . '.id = field_' . $value . '.term_id');
        $connection->addField('t_' . $value,'name',$value);
        if ($filters[$value]) {
          $connection->condition('t_' . $value . '.id',$filters[$value]);
        }
      }
    }
    if (strlen($filters['search']) > 0) {
      $connection->condition('an.name','%' . $filters['search'] . '%','LIKE');
    }
    $connection->orderBy('animal');
    $connection->groupBy('an.id');

    // echo '<div style="background-color: white;"><pre>' . var_export($connection->__toString(),TRUE) . '</pre></div>';

    $res = $connection->execute();
    $results = $connection->arrayResults($res);

    if (!empty($results)) {
      foreach ($results as $key => $row) {
        $lifestyle = $this->getLifestyle($row['id']);
        if ($lifestyle) {
          $results[$key]['lifestyle'] = array();
          foreach ($lifestyle as $ls) {
            $results[$key]['lifestyle'][] = $ls['name'];
          }
        }
      }
    }

    return $results;
  }

  public function get($id) {
    // $connection = $this->connect();

    // $connection->select('animals','an');
    // $connection->join('field_image__thumb','thumbs','thumbs.animal_id = an.id');
    // $connection->join('field_image__large','bg','bg.animal_id = an.id');
    // $connection->fields('an');
    // $connection->addField('thumbs','filename','thumb');
    // $connection->addField('bg','filename','bkgd');
    // $connection->condition('an.id',$id);
    // $connection->orderBy('name');

    // return $connection->execute();

    // echo '<div style="background-color: white;"><pre>' . var_export($filters,TRUE) . '</pre></div>';
    $filter = new Filters;
    $connection = $this->connect();

    $connection->select('animals','an');
    $connection->leftJoin('field_image__thumb','thumbs','thumbs.animal_id = an.id');
    $connection->leftJoin('field_image__large','bkgd','bkgd.animal_id = an.id');
    $connection->fields('an');
    $connection->addField('an','name','animal');
    $connection->addField('thumbs','filename','thumb');
    $connection->addField('bkgd','filename','bkgd');
    foreach ($filter->getVocabs() as $filter) {
      foreach ($filter as $value) {
        $connection->leftJoin('field_' . $value,'field_' . $value,'field_' . $value . '.animal_id = an.id');
        $connection->leftJoin('terms','t_' . $value,'t_' . $value . '.id = field_' . $value . '.term_id');
        $connection->addField('t_' . $value,'name',$value);
      }
    }
    $connection->condition('an.id',$id);
    $connection->orderBy('animal');
    $connection->groupBy('an.id');

    $res = $connection->execute();
    $results = $connection->arrayResults($res);

    if (!empty($results)) {
      foreach ($results as $key => $row) {
        $lifestyle = $this->getLifestyle($row['id']);
        if ($lifestyle) {
          $results[$key]['lifestyle'] = array();
          foreach ($lifestyle as $ls) {
            $results[$key]['lifestyle'][] = $ls['name'];
          }
        }
      }
    }

    return $results;
  }

  public function getLifestyle($id) {
    $connection = $this->connect();

    $connection->select('terms','t');
    $connection->leftJoin('field_lifestyle','lstyle','lstyle.term_id = t.id');
    $connection->fields('t',array('name'));
    $connection->condition('lstyle.animal_id',$id);

    $res = $connection->execute();
    $results = $connection->arrayResults($res);

    return $results;
  }

}