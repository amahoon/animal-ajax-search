<?php
  date_default_timezone_set('America/Toronto');
  $date = strtotime('now');

  $day = date("j", $date);
  $suffix = date("S", $date);
  $month = date("F", $date);

  $spring = strtotime('March 20 ' . date('Y'));
  $summer = strtotime('June 21 ' . date('Y'));
  $fall = strtotime('Sept 22 ' . date('Y'));
  $winter = strtotime('Dec 21 ' . date('Y'));
  
  if ($date < $spring) {
    $season = 'winter';
  } elseif ($date >= $spring && $date < $summer) {
    $season = 'spring';
  } elseif ($date >= $summer && $date < $fall) {
    $season = 'summer';
  } elseif ($date >= $fall && $date < $winter) {
    $season = 'fall';
  } else {
    $season = 'winter';
  }
