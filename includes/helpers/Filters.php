<?php

/**
 * @file
 * Creates an object with method calls for filtering results.
 */

class Filters {

  public function connect() {
    return new Connection;
  }

  public function getFilerValues() {
    $vocabs = $this->getVocabs();
    $results = array();
    foreach ($vocabs as $v) {
      $results[$v['name']] = array();
      $terms = $this->getTerms($v['name']);
      foreach ($terms as $term) {
        $results[$v['name']][$term['id']] = $term['name'];
      }
    }

    return $results;
  }

  public function getVocabs() {
    $connection = $this->connect();

    $connection->select('terms','t');
    $connection->addField('t','vocab','name');
    $connection->groupBy('vocab');

    $res = $connection->execute();
    $results = $connection->arrayResults($res);

    return $results;
  }

  public function getTerms($vocab = NULL) {
    $connection = $this->connect();

    $connection->select('terms','t');
    $connection->fields('t',array('id','name'));
    if ($vocab) {
      $connection->condition('vocab',$vocab);
    }
    $connection->orderBy('name');

    $res = $connection->execute();
    $results = $connection->arrayResults($res);

    return $results;
  }

}