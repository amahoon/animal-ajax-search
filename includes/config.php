<?php 

	$base_url = $_SERVER['DOCUMENT_ROOT'];

	$includes = array(
    '/includes/helpers/Connection.php',
    '/includes/helpers/Season.php',
    '/includes/helpers/Animals.php',
    '/includes/helpers/Filters.php'
  );
	foreach ($includes as $file) {
  	if (!in_array($base_url . $file,get_included_files())) {
      include $base_url . $file;
    }
	}