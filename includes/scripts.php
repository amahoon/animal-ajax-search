<script type="text/javascript" src="node_modules/jquery/dist/jquery.min.js"></script>
<script type="text/javascript" src="node_modules/jquery-match-height/jquery.matchHeight.js"></script>  
<script type="text/javascript" src="dist/js/utils.min.js"></script>
<!-- <script type="text/javascript" src="dist/js/display-options.min.js"></script> -->
<script type="text/javascript" src="dist/js/main.min.js"></script>
<script type="text/javascript" src="js/search-filters.js"></script>
<script type="text/javascript" src="js/animal.js"></script>

<!-- OTHER JAVASCRIPT -->
<script>
	// HIDE LOADER WHEN PAGE IS FINISHED LOADING
	window.onload = function() {
		$('body').addClass('loaded');
		window.scrollTo(0, 0);
	}
</script>